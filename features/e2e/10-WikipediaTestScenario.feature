# encoding: utf-8
# language: de

@E2E
Funktionalität: 10_Wikipedia_aufrufen_1_Szenario

  Szenario: 10-01 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie