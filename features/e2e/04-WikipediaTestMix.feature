# encoding: utf-8
# language: de

@E2E
Funktionalität: 04_Wikipedia_aufrufen_2_Szenarien

  Szenario: 04-01 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie

  Szenario: 04-02 Wikipedia Seite kann erfolgreich aufgerufen werden überspringen

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie

  Szenariogrundriss: 04-03_<iteration> Iteration bei Szenariogrundriss überspringen

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel <title>

    Beispiele:
      | iteration    | title                             |
      | iteration_01 | Wikipedia, die freie Enzyklopädie |
      | iteration_02 | Wikipedia, die freie Enzyklopädie |

    Beispiele:
      | iteration    | title                             |
      | iteration_03 | Wikipedia, die freie Enzyklopädie |
      | iteration_04 | Wikipedia, die freie Enzyklopädie |
      | iteration_05 | Wikipedia, die freie Enzyklopädie |
      | iteration_06 | Wikipedia, die freie Enzyklopädie |
      | iteration_07 | Wikipedia, die freie Enzyklopädie |
      | iteration_08 | Wikipedia, die freie Enzyklopädie |
      | iteration_09 | Wikipedia, die freie Enzyklopädie |
      | iteration_10 | Wikipedia, die freie Enzyklopädie |
      | iteration_11 | Wikipedia, die freie Enzyklopädie |
