# encoding: utf-8
# language: de

@E2E
Funktionalität: 02_Wikipedia_aufrufen_5_Szenarien

  Szenario: 02-01 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie

  Szenario: 02-02 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie

  Szenario: 02-03 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie

  Szenario: 02-04 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie

  Szenario: 02-05 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie
