# encoding: utf-8
# language: de

@E2E
Funktionalität: 05_Wikipedia_aufrufen_2_Szenarien

  Szenario: 05-01 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie

  Szenario: 05-02 Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel Wikipedia, die freie Enzyklopädie