# encoding: utf-8
# language: de

@E2E
Funktionalität: 03_Wikipedia_aufrufen_1_Szenariogrundriss_3_it

  Szenariogrundriss: 03-01_<iteration> Wikipedia Seite kann erfolgreich aufgerufen werden

    Angenommen die Startseite von Wikipedia ist aufgerufen

    Dann enthält die Startseite von Wikipedia den Titel <title>

    Beispiele:
      | iteration   | title                             |
      | iteration_1 | Wikipedia, die freie Enzyklopädie |
      | iteration_2 | Wikipedia, die freie Enzyklopädie |
      | iteration_3 | Wikipedia, die freie Enzyklopädie |
