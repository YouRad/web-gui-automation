import {Before, BeforeAll, After, AfterAll, Status, HookScenarioResult, World} from "cucumber";
import {browser} from "protractor";
import {config} from "../../protractorConfig";

BeforeAll({timeout: 20 * 1000}, async () => {
    if (config.baseUrl != null) {
        await browser.get(config.baseUrl);
    } else {
        throw new Error('Base Url is not defined')
    }
});

Before({tags: "@Foo"}, async () => {
    // This hook will be executed before scenarios tagged with @Foo
    await browser.manage().deleteAllCookies();
});

Before({tags: "@Skip"}, async () => {
    // perform some runtime check to decide whether to skip the proceeding scenario
    return 'skipped';
});

After(async function (this: World, scenario: HookScenarioResult) {
    if (scenario.result.status === Status.FAILED || scenario.result.status === Status.UNDEFINED) {
        // screenShot is a base-64 encoded PNG
        const screenShot = await browser.takeScreenshot();
        this.attach(screenShot, "image/png");
    }
});

After(async function (this: World) {
    const url = await browser.driver.getCurrentUrl();
    this.attach('Current URL: ' + url);
});

AfterAll({timeout: 10 * 1000}, async () => {
    await browser.quit();
});