import reporter from "multiple-cucumber-html-reporter";
import {reportsDir} from "../../arguments";


const jsonReportDir = `${reportsDir}/json`;
const htmlReports = `${reportsDir}/html`;

const cucumberReporterOptions = {
    jsonDir: `${jsonReportDir}/json-output-folder/`,
    reportPath: htmlReports,
    customData: {
        "App Version": "0.0.1",
        "Test Environment": "INTEGRATION",
        "Browser": "Chrome  xy",
        "Platform": "Mac",
        "Parallel": "Scenarios",
        "Executed": "LOKAL"
    }
};

export class Reporter {
    public static createHTMLReport() {
        try {
            reporter.generate(cucumberReporterOptions);
        } catch (err) {
            if (err) {
                console.error(`*** Failed to generate HTML-Report in ${cucumberReporterOptions.reportPath} ***`);
                console.error(err);
                const EXIT_FAILURE = 1;
                process.exit(EXIT_FAILURE);
            }
        }
    }
}