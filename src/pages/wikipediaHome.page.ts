import { $ } from 'protractor';
import { BasePage } from './base.page';

export class WikipediaHomePage extends BasePage {

  protected locator = $('.wikipedia-logo');

  public async init(): Promise<this> {
    await super.init();
    return this;
  }
}
