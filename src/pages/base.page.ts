import { browser, ElementFinder, ExpectedConditions } from 'protractor';

/**
 * Basisklasse für alle Page-Objects.
 *
 * Gemeinsame Funktionalität ist das Warten, bis die Seite geladen ist (siehe init()).
 */
export abstract class BasePage {

  public static readonly ACTION_SLOW_TIMEOUT_MS = 10000;


  protected abstract locator: ElementFinder;

  /** Wartet darauf, dass die Seite vollständig geladen ist. Überschreiben, wenn komplexere Logik notwendig ist. */
  public async init() {
    await this.waitForPagePresence();
    return this;
  }

  public async getTitle() {
    return browser.getTitle();
  }

  public async waitForPagePresence() {
    await browser.wait(
      ExpectedConditions.presenceOf(this.locator),
      BasePage.ACTION_SLOW_TIMEOUT_MS,
      `Locator:  ${this.locator.locator()}`);
  }

  public async refresh() {
    await browser.refresh();
    await this.init();
    return this;
  }
}
