import { expect } from 'chai';
import { Given, Then } from 'cucumber';
import { WikipediaHomePage } from '../pages/wikipediaHome.page';

const wikipedia: WikipediaHomePage = new WikipediaHomePage();
Given(/^die Startseite von Wikipedia ist aufgerufen$/, async () => {
  await wikipedia.init();
});

Then(/^enthält die Startseite von Wikipedia den Titel (.+)$/, async (title: string) => {
  expect(await wikipedia.getTitle()).to.equal(title);
});
