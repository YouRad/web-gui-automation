import appRootPath from 'app-root-path';
import {browser, Config} from "protractor";
import {baseUrl, cucumberTags, featurePath, seleniumUrl, isPipeline, reportsDir} from './arguments';
import os from "os";

const jsonReportDir = `${reportsDir}/json`;
const targetJsonReport = jsonReportDir + "/cucumber_report.json";
const htmlReports = `${reportsDir}/html`;

const maxInstances = Math.round(os.cpus().length * 0.66);

export const config: Config = {
    seleniumAddress: seleniumUrl || undefined,
    baseUrl: baseUrl,
    SELENIUM_PROMISE_MANAGER: false,
    directConnect: !seleniumUrl,
    getPageTimeout: 180000,
    allScriptsTimeout: 210000,

    capabilities: {
        maxInstances: maxInstances,
        shardTestFiles: true,
        browserName: "chrome",
        chromeOptions: {
            args: [
                '--disable-gpu',
                '--lang=de',
            ].concat(isPipeline ? '--headless' : [])
                .concat(isPipeline ? '--no-sandbox' : [])
                .concat(isPipeline ? '--disable-gpu' : []),
        }
    },
    framework: "custom",
    frameworkPath: require.resolve("protractor-cucumber-framework"),
    specs: [appRootPath.resolve(featurePath)],
    cucumberOpts: {
        format: [
            `json:${targetJsonReport}`,
            require.resolve('cucumber-pretty')
        ],
        require: [
            appRootPath.resolve('build/js/src/stepDefinitions/**/*.steps.js'),
            appRootPath.resolve('build/js/src/stepDefinitions/*.steps.js'),
            appRootPath.resolve('build/js/src/support/*.js'),
        ],
        strict: true,
        tags: cucumberTags,
    },

    plugins: [
        {
            package: 'protractor-multiple-cucumber-html-reporter-plugin',
            options: {
                automaticallyGenerateReport: true,
                removeExistingJsonReportFile: true,
                disableLog: false,
                openReportInBrowser: true,
                jsonDir: `${jsonReportDir}/json-output-folder/`,
                reportPath: htmlReports,
                pageTitle: "Cucumber E2E Tests Report",
                reportName: "E2E Tests",
                displayDuration: true,
                durationInMS: true,
                customData: {
                    title: 'Run info',
                    data: [
                        {label: 'Project', value: 'WEB GUI Testautomation'},
                        {label: 'Release', value: '0.0.0'},
                        {label: 'OS', value: os.platform()},
                        {label: 'Execution Start (UTC)', value: new Date().toISOString()},
                    ]
                }
            },
        },
    ],
    onPrepare: () => {
        browser.waitForAngularEnabled(false).then();
        browser.manage().window().maximize().then();
    },

    onComplete: () => {
      // Reporter.createHTMLReport();
    },
};