import appRootPath from 'app-root-path';

export const isPipeline = !!process.env.CI || false;

export const seleniumUrl = process.env.SELENIUM_GRID_URL;
export const baseUrl = process.env.BASE_URL || 'https://www.wikipedia.de';
export const cucumberTags = process.env.CUCUMBER_TAGS || '@E2E';
export const featurePath = process.env.FEATURE_PATH || 'features/**/*.feature';

export const reportsDir = process.env.REPORTS_DIRECTORY || appRootPath.resolve('/build/reports');