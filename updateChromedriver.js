'use strict';

if (process.env.CI) {
    console.log('***** Skipping WebDriver download in CI environment *****');
    return;
}

console.log('***** start update chrome driver *****');
const childProcess = require('child_process');
const webDriverManager = require.resolve('webdriver-manager');

childProcess.execFileSync('node', [webDriverManager, 'update', '--gecko=false'], { stdio: 'inherit' });
console.log('***** end update chrome driver *****');