# WEB GUI-Testautomatisierung mit Cucumber und Protractor

## Erforderliche Libs
- NodeJS auf System-Ebene installieren https://nodejs.org/en/download/
- Chrome Browser ist installiert

## Schritte zur Einrichtung
- Repository auschecken
- Dependencies mit folgendem Befehl installieren
```console
npm install
```
alle in package.json defenierten Dependencies sind jetzt in node_modules Ordner installiert
'postinstall' script aktualisiert chromedriver

## Tests ausführen
- Der Code muss zuerst mit fegendem Befehl kompiliert werden. `.js` Dateien werden in den Ordner `build/js` erstellt  
```console
npm run build
```
- Die Tests können mit folgendem Befehl ausgefürt werden
```console
npm run test
```
Mit directConnect='true' in Konfiguration vom Protractor wird Selenium Server automatisch gestartet
Man kann den Server auch manuell mit felgendem Befehl ausführen
```console
npm run webdrier-start
``` 
zuerst startet das Skript Chrome Browser, führt die Tests aus und anschließend wird ein HTML-Report in den Ordner `/build/Reports/html` erstellt


## Environment variables   
    - BASE_URL: die url der Applikation default `https://www.wikipedia.de`
    - CUCUMBER_TAGS: default '@E2E';
    - FEATURE_PATH: default 'features/e2e/**/*.feature';
    - SELENIUM_URL: default 'http://127.0.0.1:4444/wd/hub'
    - REPORTS_DIRECTORY: '/build/Reports'
